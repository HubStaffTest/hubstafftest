import os
from datetime import datetime, date, timedelta

from flask import Flask
from flask_cors import CORS
from flask_restful import  reqparse, Api, Resource

from api import get_employees, get_projects_list, get_all_track
from mail_sender import send_mail


app = Flask(__name__)
try:
    app.config.from_object(os.environ['APP_SETTINGS'])
except KeyError:
    print("Please set APP_SETTING environment variable.")
CORS(app, resources={r"/api/*": {"origins": "*"}})

api = Api(app)
parser = reqparse.RequestParser()
parser.add_argument('start_date', type=str)
parser.add_argument('end_date', type=str)
parser.add_argument('organizations', type=str)
parser.add_argument('projects', type=str)
parser.add_argument('employees', type=str)
parser.add_argument('receiver_email', type=str)
parser.add_argument('email_data', type=str)


class EmployeeList(Resource):
    def get(self):
        return get_employees()


class ProjectList(Resource):
    def get(self):
        return get_projects_list()


class GetAllTrack(Resource):

    @staticmethod
    def parse_employee_project_hours_from_json(json_data):
        employee_project_hours = [
            {'name': user['name'], 'project': user_project['name'], 'duration': user_project['duration']}
            for organizations in json_data.values()
            for organization in organizations
            for user in organization['users']
            for project in user['dates']
            for user_project in project['projects']]
        employee_hours = {}
        for employee in employee_project_hours:
            if employee['name'] not in employee_hours.keys():
                employee_hours[employee['name']] = {}
            if employee['project'] not in employee_hours[employee['name']].keys():
                employee_hours[employee['name']][employee['project']] = 0
            employee_hours[employee['name']][employee['project']] += employee['duration']
        return employee_hours

    def get(self):
        args = parser.parse_args()
        try:
            start_date = datetime.strptime(args['start_date'], '%Y-%m-%d')
        except ValueError:
            start_date = date.today() - timedelta(days=30)
        try:
            end_date = datetime.strptime(args['end_date'], '%Y-%m-%d')
        except ValueError:
            end_date = date.today()

        json_data = get_all_track(start_date, end_date, organizations=args['organizations'], projects=args['projects'],
                                  employees=args['employees'])
        return self.parse_employee_project_hours_from_json(json_data)


class SendReportToManager(Resource):
    def get(self):
        args = parser.parse_args()
        send_mail(args['receiver_email'], args['email_data'])
        return f"Sent successfully to {args['receiver_email']}"


api.add_resource(EmployeeList, '/api/employeelist')
api.add_resource(ProjectList, '/api/projectlist')
api.add_resource(GetAllTrack, '/api/getalltrack')
api.add_resource(SendReportToManager, '/api/send')

if __name__ == '__main__':
    app.run()
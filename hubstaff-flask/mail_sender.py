import smtplib
from datetime import datetime
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from config import SMTP_SERVER, SMTP_PORT, SMTP_SENDER, ADMIN_SMTP_PASSWORD, ADMIN_SMTP_USERNAME


def send_mail(receiver, message):
    server = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
    server.ehlo()
    server.starttls()
    server.ehlo()
    msg = MIMEMultipart('alternative')
    msg['Subject'] = f'Report - {datetime.now().strftime("%y-%m-%d %H:%M")}'
    msg['From'] = SMTP_SENDER
    msg['To'] = receiver
    template = """<html><head><style>
    .table th, .table td{padding: 0.75rem; vertical-align: top; border-top: 1px solid #dee2e6;}
    .table thead th{vertical-align: bottom; border-bottom: 2px solid #dee2e6;}
    .table tbody + tbody{border-top: 2px solid #dee2e6;}
    .table{width: 100%; max-width: 100%; margin-bottom: 1rem; background-color: transparent;}
    .table-striped tbody tr:nth-of-type(odd){background-color: rgba(0, 0, 0, 0.05);}
    .table-dark.table-striped tbody tr:nth-of-type(odd){background-color: rgba(255, 255, 255, 0.05);}
    </style></head><body>""" + message + "</body></html> "
    msg.attach(MIMEText(message, 'html'))
    server.login(ADMIN_SMTP_USERNAME, ADMIN_SMTP_PASSWORD)
    server.sendmail(SMTP_SENDER, receiver, msg.as_string())

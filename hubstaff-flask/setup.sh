#!/bin/bash

venv_dir='venv'
python_bin='/usr/bin/python'

if [ "$1" != "run" ] 
then    
    required_pkgs='python3 python3-pip python-pip libpq-dev python-dev libxml2-dev libxslt1-dev libyaml-dev libxmlsec1-dev'
    #
    # add to ${required_pkgs} additional packages if needed:
    #    rabbitmq-server   - AMQP server, it may be used for queue system with celery

    #
    # function to determine if package is installed
    #
    install_package() {
        # FIXME: if you have other locale than en_EN you must check that answer from
        #        command bellow will not be exactly 'install ok installed' in this
        #        other language
        result=$(dpkg-query --show --showformat='${Status}' $1 2> /dev/null)
        if [ "$result" != 'install ok installed' ] ; then
            sudo apt-get install -y $1
        fi
    }

    #
    # Starts script
    #

    for pkg in $required_pkgs; do
        install_package $pkg
    done

    if [ ! -f "$(which virtualenv)" ]; then
        sudo pip install virtualenv
    fi
    if [ ! -d "${venv_dir}" ]; then
        virtualenv --python=${python_bin} --no-site-packages ${venv_dir}
    fi

    #
    # Now environment is ready, activate it
    #

    pwd
    source ${venv_dir}/bin/activate

    pip3 install -r requirements.txt
    export APP_SETTINGS="config.DevelopmentConfig"
fi

if [ "$1" = "run" ] 
then
    source ${venv_dir}/bin/activate
    cd ..
    cd hubstaff-vue
    python3 -m http.server &
    cd ..
    cd hubstaff-flask
    python3 app.py
fi

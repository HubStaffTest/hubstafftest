import requests
from config import BASE_HUBSTAFF_API_URL, HEADERS


def get_employees(organization_memberships=True, project_memberships=True, offset=0,
                  organization=None, project=None):
    data = {'organization_memberships': organization_memberships, 'project_memberships': project_memberships,
            'offset': offset, 'organization': organization, 'project': project}
    return requests.get(BASE_HUBSTAFF_API_URL+'/users', data, headers=HEADERS).json()


def get_projects_list(status=None, offset=0):
    data = {'status': status, 'offset': offset}
    return requests.get(BASE_HUBSTAFF_API_URL+'/projects', data, headers=HEADERS).json()


def get_all_track(start_date, end_date, employees='', projects='', organizations=''):
    data = {'start_date': start_date.isoformat(), 'end_date': end_date.isoformat(), 'projects': projects,
            'organizations': organizations, 'users': employees}
    return requests.get(BASE_HUBSTAFF_API_URL+'/custom/by_member/my', data, headers=HEADERS).json()

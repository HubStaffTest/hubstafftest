var app = new Vue({
    el: '#employeeTable',
    components: {
        vuejsDatepicker
    },
    data: {
        employees: [],
        projects: [],
        selectedEmployees: [],
        selectedProjects: [],
        startDate: moment().subtract(7, 'days').toDate(),
        endDate: new Date(),
        data: [],
        disabledDates: {to: moment().subtract(31, 'days').toDate()},
        disabledDatesEnd: {to: moment().subtract(7, 'days').toDate()},
        email: ''
    },
    methods: {
        dateFormatter (date) {
            return moment(date).format('YYYY-MM-DD')
        },
        changeData () {
            axios
                .get('http://localhost:5000/api/getalltrack', {
                    params: {
                        start_date: this.dateFormatter(this.startDate),
                        end_date: this.dateFormatter(this.endDate),
                        employees: this.selectedEmployees.join(','),
                        projects: this.selectedProjects.join(','),
                    }
                })
                .then(response => {this.data = response.data})
        },
        sendEmail () {
            axios
                .get('http://localhost:5000/api/send', {
                    params: {
                        email_data: document.getElementById("resultTable").innerHTML,
                        receiver_email: this.email
                    }
                })
                .then(alert("Sent successfully."))
        },
        projectsCondition (project) {
            if (this.selectedProjects.length == 0) {
                return true
            }
            return this.selectedProjects.includes(project['id'])
        },
        selectAllProjects() {
            this.selectedProjects = []
            this.changeData()
        },
        selectAllEmployees() {
            this.selectedEmployees = []
            changeData()
        }
    },
    filters : {
        round: function (v) {
            if (!v || v == 0) return 0
            return v.toFixed(2)
        }
    },
    mounted: function getEmployees () {
        axios
            .get('http://localhost:5000/api/employeelist')
            .then(response => (this.employees = response.data))
        axios
            .get('http://localhost:5000/api/projectlist')
            .then(response => (this.projects = response.data['projects']))
        this.changeData()
    }
})